# The Static Site Generator (ssg)

[![pipeline status](https://gitlab.com/malek4/genie_logiciel_project/badges/master/pipeline.svg)](https://gitlab.com/malek4/genie_logiciel_project/-/commits/master)
[![coverage report](https://gitlab.com/malek4/genie_logiciel_project/badges/master/coverage.svg)](https://gitlab.com/malek4/genie_logiciel_project/-/commits/master)
[![download ](https://img.shields.io/amo/users/dowload?label=download)](https://gitlab.com/malek4/genie_logiciel_project/-/jobs/artifacts/master/download?job=package)
[![picocli](https://img.shields.io/badge/picocli-4.6.1-green.svg)](https://github.com/remkop/picocli)

Command line application for generating a static website from a predefined Markdown files.

The specification of Markdown used is [CommonMark](https://github.com/commonmark/commonmark-java).

## Project Architecture
[architecture](architecture.md)

## Getting started

### Download

- Use the download badge above (NOTE: it will work even with `not found`) to get the latest version of ssg.

- The archive contains:
    ```
    .
    ├── README.md
    ├── ssg
    └── ssg.jar

    0 directories, 3 files
    ```

### Quick guide

1. Make sure you are at the same folder that contains the *ssg.jar*

1. Usage :

- Get a help
    ```shell
    > ./ssg help [COMMAND]
    ```
  
- Translate Markdown file(s) to HTML 
    ```shell
    > ./ssg file1.html file2.html ... [--output-dir DIR]
    ```
   
    file1.html file2.html are a list of files  to produce.
   
- Generate an entire site
    ```shell
    > ./ssg [COMMAND] [--input-dir DIR] [--output-dir DIR]
    ```
   
` [COMMAND]`  

- **help** :    Displays help information about the specified command.
   
- **build** : Generate the HTML files or build a entire site.
   ```shell
    > ./ssg build [--watch] [--input-dir DIR] [--output-dir DIR]
    ```
      [--watch] : monitor source files and automatically recompiles the site with each modification 
   
- **serve** : Generate the HTML files or build a entire site and then start an HTTP server for it.


The HTML files are produced in the `_output` directory by default.
When building a site, the current directory is used as  `input` directory by default.
The `input` directory must have a directory named `content` which also must have an `index.md` file.
In case of template, the templates directory must have a `default.html` file.

## External Libraries

### Dependencies:
   
- [commonmark-java](https://github.com/commonmark/commonmark-java)

- [picocli](https://github.com/remkop/picocli)

- [TomlJ](https://github.com/tomlj/tomlj)

- [jinjava](https://github.com/HubSpot/jinjava)


### Testing libraries:

- [Junit5](https://junit.org/junit5/docs/current/user-guide/#overview)

- [mockito](https://site.mockito.org/)

- [jacoco](https://github.com/jacoco/jacoco)
