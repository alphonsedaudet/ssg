+++
title = "SSG - about"
template = "about.html"
+++

Il s’agit d’un outil capable de traduire un ensemble de fichiers écrits dans un
format de document de haut niveau (type Markdown) vers HTML, CSS et éventuellement
JavaScript.