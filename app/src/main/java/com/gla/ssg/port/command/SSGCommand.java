package com.gla.ssg.port.command;

import com.gla.ssg.domain.renderer.HTMLRenderer;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import java.util.concurrent.Callable;

@Command(name = "ssg", mixinStandardHelpOptions = true, version = "ssg v1.0", commandListHeading = "\nCommands:\n", description = "\nCommand line application for generating a static website from a predefined Markdown files.\n")
public class SSGCommand implements Callable<Integer> {

    private final HTMLRenderer renderer;
    private String[] args;

    public SSGCommand(HTMLRenderer renderer, String[] args) {
        this.renderer = renderer;
        this.args = args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }

    @Override
    public Integer call() {
        new CommandLine(this).printVersionHelp(System.out);
        return 0;
    }

    public int start() {
        CommandLine commandLine = new CommandLine(this).addSubcommand(CommandLine.HelpCommand.class)
                .addSubcommand("build", new BuildCommand(renderer))
                .addSubcommand("serve", new ServeCommand(renderer));
        return commandLine.execute(args);
    }
}
