package com.gla.ssg.port.watchdog;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;

import com.gla.ssg.domain.generator.Generator;
import com.gla.ssg.domain.generator.WatchObserver;

public class Watchdog {

	private final Path inputDir;
	private final Path inputDir_content;
	private final WatchObserver generator;

	private Watchdog(Generator generator) {
		this.generator = generator;
		this.inputDir = generator.getInputDir();
		this.inputDir_content = Paths.get(inputDir.toString().concat("/content"));
		System.out.println(inputDir_content);
	}

	/**
	 * Async function
	 */
	public static void subscribeWatcher(WatchObserver observer) {
		new Thread(() -> {
			try {
				new Watchdog((Generator) observer).watch();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	@SuppressWarnings("unchecked")
	private void watch() throws Exception {

		WatchService watchService;
		WatchKey key;
		Path fullPath;
		String eventKind;

		try {
			watchService = FileSystems.getDefault().newWatchService();
			registerPathFolder(watchService, inputDir_content);
			registerPathFolder(watchService, inputDir);

			System.out.println(" * verbose mode: verbose \n"
					+ " * automatic recompilation mode (files watcher): true / (Press CTRL+C to quit)");

			while ((key = watchService.take()) != null) {
				for (WatchEvent<?> event : key.pollEvents()) {
					eventKind = event.kind().name();

					if (event.context().toString().contains(".")) {
						fullPath = ((Path) key.watchable()).resolve(((WatchEvent<Path>) event).context());
						switch (eventKind) {

						case "ENTRY_CREATE": {
							generator.notifyChange(fullPath, WatchCallbackEvent.ENTRY_CREATE);
							break;
						}
						case "ENTRY_DELETE": {
							generator.notifyChange(fullPath, WatchCallbackEvent.ENTRY_DELETE);
							break;
						}
						case "ENTRY_MODIFY": {
							generator.notifyChange(fullPath, WatchCallbackEvent.ENTRY_MODIFY);
							break;
						}
						default:
							throw new IllegalArgumentException("Unexpected value: " + key);
						}
					}
				}
				key.reset();
			}

		} catch (IOException | InterruptedException e) {
			System.out.println(e.getMessage());
		}
	}

	private void registerPathFolder(WatchService watcher, Path pathFolder) throws IOException {
		if (!pathFolder.toFile().isDirectory() && watcher != null)
			return;
		pathFolder.register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE,
				StandardWatchEventKinds.ENTRY_MODIFY);
	}

}
