package com.gla.ssg.port.watchdog;


public enum WatchCallbackEvent{
	ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY
}
	