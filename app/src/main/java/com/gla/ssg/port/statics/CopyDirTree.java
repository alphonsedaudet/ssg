package com.gla.ssg.port.statics;

import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;


public final class CopyDirTree {


    /**
     * verifier si les sous-repertoire existent dans le repertoire destination (toDir)
     */
    private static boolean shouldCopy(Path fromDir, Path toDir) {
        boolean fromDirExistsAndIsDir = fromDir.toFile().isDirectory();
        boolean toIsBelowFrom = toDir.toAbsolutePath().startsWith(fromDir.toAbsolutePath());

        return fromDirExistsAndIsDir && !toIsBelowFrom;
    }

    public void copyDirTree(Path fromDir, Path toDir, CopyOption... opts) throws IOException {
        if (shouldCopy(fromDir, toDir)) {
            Files.walkFileTree(fromDir, new CopyVisitor(fromDir, toDir, opts));
        }
    }

}
