package com.gla.ssg.port.command;

import java.net.ServerSocket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Callable;

import com.gla.ssg.domain.generator.Generator;
import com.gla.ssg.domain.generator.GeneratorFiles;
import com.gla.ssg.domain.generator.GeneratorSite;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.port.http.HttpServer;
import com.gla.ssg.port.watchdog.Watchdog;

import picocli.CommandLine;
import picocli.CommandLine.Option;

@CommandLine.Command(name = "serve", mixinStandardHelpOptions = true, version = "1.0", headerHeading = "Usage: ", synopsisHeading = "%n", parameterListHeading = "Parameters:%n", optionListHeading = "%nOptions:%n", header = "Lancer un serveur HTTP pour le site web.", description = "")
public class ServeCommand implements Callable<Integer> {
	private final HTMLRenderer renderer;
	@CommandLine.Option(names = { "-i",
			"--input-dir" }, description = "The directory that contains the files to be processed.")
	private final Path inputDir = Paths.get(System.getProperty("user.dir"));
	@CommandLine.Option(names = { "-o",
			"--output-dir" }, description = "The directory where to produce the HTML files.\n"
					+ "By default the files are produced in the directory _output/")
	private final Path outputDir = Paths.get("_output");
	@CommandLine.Parameters(description = "List of HTML files to produce.")
	private final List<Path> files = Collections.emptyList();
	@CommandLine.Option(names = { "-p", "--port" }, description = "Port number.")
	private Integer port = 8080;

	@CommandLine.Option(names = { "--verbose" }, description = "Verbose mode.")
	private final Boolean verbose = false;

	@Option(names = { "--rebuild-all " }, description = "rebuild all mode.")
	private final Boolean rebuildAll = false;

	public ServeCommand(HTMLRenderer renderer) {
		this.renderer = renderer;
	}

	@Override
	public Integer call() throws Exception {
		if (!Files.exists(outputDir))
			Files.createDirectory(outputDir);
		try {
			Generator generator;
			if (files.size() > 0)
				generator = new GeneratorFiles(renderer, outputDir, files, rebuildAll);
			else {
				generator = new GeneratorSite(renderer, outputDir, inputDir, rebuildAll);
			}
			Watchdog.subscribeWatcher(generator);
			generator.run();
			System.out.println(" * verbose mode: " + verbose + "\n" + " * Running on http://0.0.0.0:" + port
					+ "/ (Press CTRL+C to quit)");
			ServerSocket serverConnect = new ServerSocket(port);
			while (true) {
				if (verbose)
					System.out.println("waiting for connection...");
				final HttpServer httpServer = new HttpServer(serverConnect.accept(), outputDir, verbose);
				if (verbose)
					System.out.println("New connection [ " + new Date() + " ]");
				new Thread(httpServer).start();
			}
		} catch (Exception e) {
			System.err.println("ssg error: " + e.getMessage());
		}
		return 0;
	}
}
