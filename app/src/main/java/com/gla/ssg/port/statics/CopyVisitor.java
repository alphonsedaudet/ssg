package com.gla.ssg.port.statics;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

final class CopyVisitor extends SimpleFileVisitor<Path> {
    private final Path fromDir;
    private final Path toDir;
    private final CopyOption[] opts;

    CopyVisitor(Path fromDir, Path toDir, CopyOption... opts) {
        this.fromDir = fromDir;
        this.toDir = toDir;
        this.opts = opts;
    }

    /**
     * Création des repertoires
     * <ul>
     * <li> Créer les sous-répertoires inexistants, si nécessaire.
     * <li> si le répertoire existe déja --> ne rien faire et aucune exception n'est levée
     * </ul>
     */
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        Files.createDirectories(toDir.resolve(fromDir.relativize(dir)));
        return FileVisitResult.CONTINUE;
    }

    /**
     * Copier tous les fichiers du repertoire
     */
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        Files.copy(file, toDir.resolve(fromDir.relativize(file)), opts);
        return FileVisitResult.CONTINUE;
    }

}