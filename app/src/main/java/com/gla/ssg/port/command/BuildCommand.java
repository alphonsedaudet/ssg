package com.gla.ssg.port.command;

import com.gla.ssg.domain.generator.Generator;
import com.gla.ssg.domain.generator.GeneratorFiles;
import com.gla.ssg.domain.generator.GeneratorSite;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.port.watchdog.Watchdog;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

@Command(name = "build", mixinStandardHelpOptions = true, version = "1.0", headerHeading = "Usage: ", synopsisHeading = "%n", parameterListHeading = "Parameters:%n", optionListHeading = "%nOptions:%n", header = "Generate the HTML files or build a entire site.", description = "")
public class BuildCommand implements Callable<Integer> {

	private final HTMLRenderer renderer;

	@Option(names = { "-i", "--input-dir" }, description = "The directory that contains the files to be processed.")
	private final Path inputDir = Paths.get(System.getProperty("user.dir"));

	@Option(names = { "-o", "--output-dir" }, description = "The directory where to produce the HTML files.\n"
			+ "By default the files are produced in the directory _output/")
	private final Path outputDir = Paths.get("_output");

	@Parameters(description = "List of HTML files to produce.")
	private final List<Path> files = Collections.emptyList();

	@Option(names = { "--rebuild-all " }, description = "rebuild all mode.")
	private final Boolean rebuildAll = false;

	@Option(names = { "--watch" }, description = "Watch mode.")
	private final Boolean watch = false;

	@Option(names = { "--verbose" }, description = "Verbose mode.")
	private final Boolean verbose = false;

	public BuildCommand(HTMLRenderer renderer) {
		this.renderer = renderer;
	}

	@Override
	public Integer call() throws Exception {
		if (!Files.exists(outputDir))
			Files.createDirectory(outputDir);
		try {

			Generator generator;
			if (files.size() > 0)
				generator = new GeneratorFiles(renderer, outputDir, files, rebuildAll);
			else {
				generator = new GeneratorSite(renderer, outputDir, inputDir, rebuildAll);
			}
			if (watch) {
				Watchdog.subscribeWatcher(generator);
			}
			generator.run();
			if (watch) {
				while(true);
			}
			return 0;

		} catch (Exception e) {
			System.err.println("ssg error : " + e.getMessage());
			return -1;
		}
	}
}
