package com.gla.ssg.port.statics;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

//StaticHandler
public class StaticHandler {
    static CopyOption[] CAN_OVERWRITE = {
            StandardCopyOption.REPLACE_EXISTING,
            StandardCopyOption.COPY_ATTRIBUTES,
            LinkOption.NOFOLLOW_LINKS //you likely want this
    };

    public static void copyTree(String from, String to) throws IOException {

        CopyDirTree cdt = new CopyDirTree();
        cdt.copyDirTree(Paths.get(from), Paths.get(to), CAN_OVERWRITE);

    }

    public static Boolean copierFichier(String sourcePath, String destinationPath) throws IOException {
        Path source = Paths.get(sourcePath);

        String fileName;
        if (sourcePath.contains("/"))
            fileName = sourcePath.substring(sourcePath.lastIndexOf(File.separator));
        else
            fileName = sourcePath;
        Path dest = Paths.get(destinationPath + File.separator + fileName);
        return copierFichier(source, dest);
    }

    public static Boolean copierFichier(Path sourcePath, Path destinationPath) throws IOException {
        Files.copy(sourcePath, destinationPath, REPLACE_EXISTING);
        return true;
    }

}
