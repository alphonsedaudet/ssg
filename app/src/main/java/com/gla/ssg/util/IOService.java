package com.gla.ssg.util;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

public class IOService {

    public static String readFromFile(Path path) throws IOException {
        String result = "";
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path.toFile()))) {
            StringBuilder stringBuilder = new StringBuilder();
            while ((result = bufferedReader.readLine()) != null) {
                stringBuilder.append(result).append("\n");
            }
            result = stringBuilder.toString();
        }
        return result;
    }

    public static void writeToFile(Path path, String content) throws IOException {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(path.toFile()))) {
            byte[] buf = content.getBytes(StandardCharsets.UTF_8);
            bos.write(buf);
        }
    }

    public static Optional<Path> findFile(String name, File directory) throws IOException {
        try (Stream<Path> files = Files.walk(Paths.get(directory.getPath()))) {
            return files
                    .filter(f -> f.getFileName().toString().equals(name))
                    .findFirst();
        }
    }

    public static List<String> getFiles(File dir, Boolean rec) {
        List<String> res = new ArrayList<>();
        final File[] files = dir.listFiles();
        for (File file : files) {
            if (file.isFile()) {
                res.add(file.getPath());
            } else if (file.isDirectory() && rec) {
                res.addAll(Objects.requireNonNull(getFiles(file, true)));
            }
        }
        return res;
    }
}
