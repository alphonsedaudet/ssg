package com.gla.ssg.util;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Helpers {

    static public Path changeFileExtension(Path path, String newExtension) throws IllegalArgumentException {
        String fileName = path.getFileName().toString();
        String pathStr = path.toString();
        int index = fileName.lastIndexOf('.');
        if (index > 0) {
            String nameWithNewExtension = fileName.substring(0, index) + newExtension;
            return Paths.get(pathStr.substring(0, pathStr.lastIndexOf(fileName)) + nameWithNewExtension);
        } else {
            return Paths.get(pathStr + newExtension);
        }
    }

    public static Path changeDirectory(Path path, Path newDirectory) {
        String newDirectoryStr = newDirectory.toFile().getAbsoluteFile().toString();
        return Paths.get(newDirectoryStr + File.separator + path.getFileName().toString());
    }

    public static boolean isValidUrl(String url) {
        try {
            new URL(url).toURI();
            return true;
        } catch (MalformedURLException | URISyntaxException e) {
            return false;
        }
    }

}
