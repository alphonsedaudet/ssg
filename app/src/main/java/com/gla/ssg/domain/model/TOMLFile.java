package com.gla.ssg.domain.model;

import java.nio.file.Path;
import java.util.Optional;

public class TOMLFile extends FileWrapper {

    public TOMLFile(Path path, String content) {
        super(path, content);
    }

    @Override
    protected void validatePath(Path path) throws IllegalArgumentException {
        if (!path.getFileName().toString().endsWith(".toml"))
            throw new IllegalArgumentException(path.getFileName() + " is not a TOML file");
    }

}
