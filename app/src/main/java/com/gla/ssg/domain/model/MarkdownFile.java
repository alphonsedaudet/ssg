package com.gla.ssg.domain.model;

import org.tomlj.TomlParseResult;

import java.nio.file.Path;
import java.util.Optional;

public final class MarkdownFile extends FileWrapper {

    private TOMLFile metadata;
    private Optional<TomlParseResult> metadataParseResult;
    private Optional<TomlParseResult> configParseResult;
    private Optional<HTMLFile> templateFileWrapper;

    public MarkdownFile(Path path, String content) {
        super(path, content);
        metadataParseResult = Optional.empty();
        configParseResult = Optional.empty();
        templateFileWrapper = Optional.empty();
    }

    @Override
    protected void validatePath(Path path) throws IllegalArgumentException {
        if (!path.getFileName().toString().endsWith(".md"))
            throw new IllegalArgumentException(path.getFileName() + " is not a Markdown file");
    }

    public TOMLFile getMetadata() {
        return metadata;
    }

    public void setMetadata(TOMLFile metadata) {
        this.metadata = metadata;
    }

    public Optional<TomlParseResult> getMetadataParseResult() {
        return metadataParseResult;
    }

    public void setMetadataParseResult(Optional<TomlParseResult> metadataParseResult) {
        this.metadataParseResult = metadataParseResult;
    }

    public Optional<TomlParseResult> getConfigParseResult() {
        return configParseResult;
    }

    public void setConfigParseResult(Optional<TomlParseResult> configParseResult) {
        this.configParseResult = configParseResult;
    }

    public Optional<HTMLFile> getTemplateFileWrapper() {
        return templateFileWrapper;
    }

    public void setTemplateFileWrapper(Optional<HTMLFile> templateFileWrapper) {
        this.templateFileWrapper = templateFileWrapper;
    }

}
