package com.gla.ssg.domain.generator;

import com.gla.ssg.domain.metadata.MetaData;
import com.gla.ssg.domain.model.FileWrapper;
import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.util.Helpers;
import com.gla.ssg.util.IOService;
import org.tomlj.TomlParseResult;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class Generator implements WatchObserver {

	protected HTMLRenderer renderer;
	
	protected Boolean rebuildAll = false;

	public abstract void run() throws Exception;

	public abstract Path getInputDir();

	public HTMLRenderer getHTMLRenderer() {
		return renderer;
	}

	/**
	 * Reads the markdown files corresponding to the given paths.
	 *
	 * @param paths the list of the html files paths to produce.
	 * @return the list of MarkdownFile.
	 */
	public List<MarkdownFile> readMarkdownFiles(List<Path> paths) throws Exception {
		List<MarkdownFile> list = new ArrayList<>();
		for (Path path : paths) {
			list.add(readMarkdownFile(path));
		}
		return list;
	}

	/**
	 * Renders each MarkdownFile from the given list to a HTMLFile.
	 *
	 * @param markdownFiles the list of MarkdownFile.
	 * @return the list of the corresponding HTMLFile.
	 */
	public List<HTMLFile> generateHTMLFiles(List<MarkdownFile> markdownFiles) {
		return markdownFiles.stream().map(this::generateHTMLFile).collect(Collectors.toList());
	}

	/**
	 * Writes each file from the given list to its specific path. The path is the
	 * path attribute of the FileWrapperImpl.
	 *
	 * @param files     the list of FileWrapper
	 * @param outputDir the output directory
	 */
	public void writeHTMLFiles(List<HTMLFile> files, Path outputDir) throws IOException {
		for (FileWrapper file : files)
			writeFile(file, outputDir);
	}

	public boolean isNotDraft(MarkdownFile mdFile) {
		final Optional<TomlParseResult> TOMLParseResult = mdFile.getMetadataParseResult();
		if (TOMLParseResult.isEmpty())
			return true;
		final Boolean draft = TOMLParseResult.get().getBoolean("draft");
		return draft == null || !draft;
	}

	public MarkdownFile readMarkdownFile(Path path) throws Exception {
		if (!(path.getFileName().toString().endsWith(".md")))
			throw new IllegalArgumentException(path + " should be a Markdown file with .md extension.");
		String content = IOService.readFromFile(path);
		final MarkdownFile mdFile = FileWrapperFactory.newMarkdownFile(path, content);
		MetaData.extractMetadata(mdFile);
		return mdFile;
	}

	protected HTMLFile generateHTMLFile(MarkdownFile markdownFile) {
		return renderer.markdownToHTML(markdownFile);
	}

	protected HTMLFile readMarkdownAndGenerateHtml(Path path) throws Exception {
		HTMLFile htmlFile = generateHTMLFile(readMarkdownFile(path));
		return htmlFile;
	}

	protected void writeFile(FileWrapper file, Path outputDir) throws IOException {
		IOService.writeToFile(Helpers.changeDirectory(file.getPath(), outputDir), file.getContent());
	}
}
