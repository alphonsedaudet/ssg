package com.gla.ssg.domain.template;

import com.gla.ssg.domain.metadata.MetaData;
import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.util.Helpers;
import com.gla.ssg.util.IOService;
import com.google.common.collect.Maps;
import com.hubspot.jinjava.Jinjava;
import com.hubspot.jinjava.interpret.RenderResult;
import com.hubspot.jinjava.lib.fn.ELFunctionDefinition;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Template {

    private static MarkdownFile markdownFile; // used in listFiles, static because of Jinjava
    private HTMLFile templateFile;
    private String HTMLContent;

    private String templateContent;

    public Template() { }

    /**
     * Used by Jinjava.
     */
    static List<String> listFiles(String path, Boolean rec) {
        String str = markdownFile.getPath().toString();
        final String pathToContent = str.substring(0, str.indexOf("content") + "content".length());
        String pathname = pathToContent + File.separator + path;
        File dir = new File(pathname);
        if (!dir.exists() || !dir.isDirectory()) return Collections.emptyList();
        final List<String> listFiles = IOService.getFiles(dir, rec);
        return listFiles.stream()
                .map(e -> e.substring(e.lastIndexOf("/") + 1))
                .collect(Collectors.toList());
    }

    public String getTemplateContent() {
        return templateContent;
    }

    public void setTemplateFile(HTMLFile templateFile) {
        this.templateFile = templateFile;
        this.templateContent = templateFile.getContent().trim().replaceAll(" +", " ");
    }

    public void setHTMLContent(String HTMLContent) {
        this.HTMLContent = HTMLContent;
    }

    public void setMarkdownFile(MarkdownFile md) {
        markdownFile = md;
    }

    public HTMLFile build() throws Exception {
        HTMLFile file = FileWrapperFactory.newHTMLFile(
                Helpers.changeFileExtension(markdownFile.getPath(), ".html"));
        if (templateFile.getContent().isEmpty() || markdownFile.getContent().isEmpty())
            return file;
        setInclude();
        file.setContent(renderTemplate(getBindings()));
        return file;
    }

    /**
     * Must be called after {@link #setInclude() setInclude}.
     */
    private String renderTemplate(Map<String, Object> bindings) throws Exception {
        final Jinjava jinjava = new Jinjava();
        jinjava.getGlobalContext().registerFunction(new ELFunctionDefinition(
                "",
                "list_files",
                Template.class,
                "listFiles", String.class, Boolean.class));
        RenderResult renderResult = jinjava.renderForResult(templateContent, bindings);
        if (renderResult == null) return null;
        if (!renderResult.hasErrors()) return renderResult.getOutput();
        StringBuilder str = new StringBuilder();
        renderResult.getErrors().forEach(templateError ->
                str.append(String.format("%s %s (line %d, file %s)\n",
                        templateError.getMessage(),
                        templateError.getFieldName() == null ? "" : templateError.getFieldName(),
                        templateError.getLineno(),
                        templateFile.getPath())));
        throw new Exception(str.toString());
    }

    public Map<String, Object> getBindings() {
        Map<String, Object> context = Maps.newHashMap();
        context.put("metadata", getData());
        context.put("content", HTMLContent);
        return context;
    }

    public Map<String, Object> getData() {
        boolean hasMetadata = markdownFile.getMetadataParseResult().isPresent();
        boolean hasConfigData = markdownFile.getConfigParseResult().isPresent();
        Map<String, Object> data = Maps.newHashMap();
        if (!hasMetadata && !hasConfigData)
            return data;
        if (hasConfigData)
            data.putAll(MetaData.getData(markdownFile.getConfigParseResult().get()));
        if (hasMetadata)
            data.putAll(MetaData.getData(markdownFile.getMetadataParseResult().get()));
        return data;
    }


    /**
     * Copy the files invoked by the command {@code include}. Example:
     * <pre>&lt;div id="menu"&gt;{{ include "menu.html" }}&lt;/div&gt;</pre>
     */
    public void setInclude() throws Exception {
        Matcher matcherInclude = Pattern.compile("\\{\\{ include \"(\\S*?)\" }}").matcher(templateContent);
        String label;
        Path filePath, templatePath = templateFile.getPath();
        StringBuilder content = new StringBuilder(templateContent);
        while (matcherInclude.find()) {
            label = matcherInclude.group(1);
            filePath = Paths.get(templatePath.toString().substring(0,
                    templatePath.toString().lastIndexOf(templatePath.getFileName().toString())) + label);
            if (!Files.exists(filePath))
                throw new Exception("Include failure : " + filePath + " not found");
            content.replace(matcherInclude.start(), matcherInclude.end(), IOService.readFromFile(filePath));
        }
        templateContent = content.toString();
    }

}
