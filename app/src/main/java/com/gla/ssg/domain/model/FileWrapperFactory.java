package com.gla.ssg.domain.model;

import java.nio.file.Path;

public class FileWrapperFactory {
    public static HTMLFile newHTMLFile(Path path, String content) {
        return new HTMLFile(path, content);
    }

    public static HTMLFile newHTMLFile(Path path) {
        return FileWrapperFactory.newHTMLFile(path, "");
    }

    public static MarkdownFile newMarkdownFile(Path path, String content) {
        return new MarkdownFile(path, content);
    }

    public static MarkdownFile newMarkdownFile(Path path) {
        return FileWrapperFactory.newMarkdownFile(path, "");
    }

    public static TOMLFile newTOMLFile(Path path, String content) {
        return new TOMLFile(path, content);
    }

    public static TOMLFile newTOMLFile(Path path) {
        return FileWrapperFactory.newTOMLFile(path, "");
    }
}
