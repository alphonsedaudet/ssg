package com.gla.ssg.domain.model;

import java.nio.file.Path;

public abstract class FileWrapper {
    protected Path path;
    protected String content;

    protected FileWrapper(Path path, String content) throws IllegalArgumentException {
        validatePath(path);
        this.path = path;
        this.content = content;
    }

    protected abstract void validatePath(Path path) throws IllegalArgumentException;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) throws IllegalArgumentException {
        validatePath(path);
        this.path = path;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
