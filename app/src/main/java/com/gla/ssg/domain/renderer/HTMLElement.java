package com.gla.ssg.domain.renderer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

class HTMLElement {
    private final String newLine = System.lineSeparator();
    /**
     * refer to https://www.w3.org/TR/2021/SPSD-html52-20210128/syntax.html#void-elements
     */
    private final List<String> voidHTMLTags = List.of("area", "base", "br", "col", "embed", "hr", "img",
            "input", "link", "meta", "param", "source", "track", "wbr");
    private String name, text;
    private List<HTMLElement> children = new ArrayList<>();
    private int indentSize = 0;
    private List<Map.Entry<String, String>> attributes = new ArrayList<>();

    public HTMLElement() {
    }

    public HTMLElement(String name) {
        this.name = name;
    }

    public HTMLElement(String name, String text) {
        this.name = name;
        this.text = text;
    }

    public int getIndentSize() {
        return indentSize;
    }

    public void setIndentSize(int indentSize) {
        this.indentSize = indentSize;
    }

    public List<Map.Entry<String, String>> getAttributes() {
        return attributes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<HTMLElement> getChildren() {
        return children;
    }

    private String toStringAux(int indent) {
        String i = String.join("", Collections.nCopies(indent * indentSize, " "));
        StringBuilder attr = new StringBuilder();
        attributes.forEach(e -> attr.append(String.format(" %s=\"%s\"", e.getKey(), e.getValue())));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s<%s%s>", i, name, attr));
        if (voidHTMLTags.contains(name)) return stringBuilder.append(newLine).toString();

        if (text != null && !text.isEmpty() || children.isEmpty()) {
            stringBuilder.append(text);
            children.forEach(child -> stringBuilder.append(child.toStringAux(indent + 1)));
            return stringBuilder.append(String.format("</%s>%s", name, newLine)).toString();
        } else {
            stringBuilder.append(newLine);
            children.forEach(child -> stringBuilder.append(child.toStringAux(indent + 1)));
            return stringBuilder.append(String.format("%s</%s>%s", i, name, newLine)).toString();
        }
    }

    @Override
    public String toString() {
        return toStringAux(0);
    }
}
