package com.gla.ssg.domain.renderer;

class HTMLDocument {
    private final String docType;
    private final HTMLElement root;

    public HTMLDocument(String docType, HTMLElement root) {
        this.docType = docType;
        this.root = root;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                .append(docType)
                .append(System.lineSeparator())
                .append(root.toString())
                .toString();
    }
}
