package com.gla.ssg.domain.metadata;

import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.domain.model.TOMLFile;
import com.gla.ssg.util.Helpers;
import com.google.common.collect.Maps;
import org.tomlj.Toml;
import org.tomlj.TomlParseError;
import org.tomlj.TomlParseResult;
import org.tomlj.TomlTable;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class MetaData {
    /**
     * Extract the metadata from the markdown file:
     * <ul>
     * <li> initialize the metadata file wrapper
     * <li> initialize the metadata parse result
     * <li> Remove the metadata section from the file content
     * </ul>
     *
     * @param markdownFile a markdown file
     * @throws Exception if there is a parsing error
     */
    public static void extractMetadata(MarkdownFile markdownFile) throws Exception {
        String content = markdownFile.getContent();
        Matcher matcher = Pattern.compile("\\+\\+\\+([\\S\\s]*?)\\+\\+\\+").matcher(content);
        if (!matcher.find())
            return;
        markdownFile.setMetadata(FileWrapperFactory
                .newTOMLFile(
                        Helpers.changeFileExtension(markdownFile.getPath(), ".toml"),
                        matcher.group(1)));
        markdownFile.setMetadataParseResult(Optional.of(parseTOMLFile(markdownFile.getMetadata())));
        markdownFile.setContent(content.substring(matcher.group().length()));
    }

    public static List<String> getHTMLAttributeValues(String attributeName, HTMLFile htmlFile) {
        List<String> values = new ArrayList<>();
        Pattern pattern = Pattern.compile(attributeName + "=[\"']([\\S\\s]*?)[\"']");
        Matcher matcher = pattern.matcher(htmlFile.getContent());
        while (matcher.find())
            values.add(matcher.group(1));
        return values;
    }

    public static TomlParseResult parseTOMLFile(TOMLFile TOMLFile) throws Exception {
        Path tempFile = Files.createTempFile(null, null);
        Files.writeString(tempFile, TOMLFile.getContent());
        TomlParseResult result = Toml.parse(tempFile);
        if (result.hasErrors()) {
            String join = result.errors().stream().map(TomlParseError::toString).collect(Collectors.joining("\n"));
            throw new Exception(join + " in file " + TOMLFile.getPath());
        }
        return result;
    }

    public static Map<String, Object> getData(TomlTable tomlParseResult) {
        Map<String, Object> configs = Maps.newHashMap();
        final Set<String> strings = tomlParseResult.keySet();
        for (String str : strings) {
            if (tomlParseResult.isTable(str)) {
                configs.put(str, getData(Objects.requireNonNull(tomlParseResult.getTable(str))));
            } else if (tomlParseResult.isArray(str)) {
                final List<Object> collect = Objects.requireNonNull(tomlParseResult.getArray(str)).toList().stream()
                        .map(elem -> getData((TomlTable) elem)).collect(Collectors.toList());
                configs.put(str, collect);
            } else
                configs.put(str, tomlParseResult.get(str));
        }
        return configs;
    }
}
