package com.gla.ssg.domain.generator;

import java.nio.file.Path;

import com.gla.ssg.port.watchdog.WatchCallbackEvent;

public interface WatchObserver {
    void notifyChange(Path fileChanged, WatchCallbackEvent event) throws Exception;
}
