package com.gla.ssg.domain.model;

import java.nio.file.Path;

public final class HTMLFile extends FileWrapper {

	public HTMLFile(Path path, String content) {
		super(path, content);
	}

	@Override
	protected void validatePath(Path path) throws IllegalArgumentException {
		if (!path.getFileName().toString().endsWith(".html"))
			throw new IllegalArgumentException(path.getFileName() + " is not a HTML file");
	}
}
