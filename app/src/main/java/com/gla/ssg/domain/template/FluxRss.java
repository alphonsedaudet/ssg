package com.gla.ssg.domain.template;

import com.gla.ssg.domain.model.TOMLFile;
import org.tomlj.Toml;
import org.tomlj.TomlArray;
import org.tomlj.TomlParseResult;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FluxRss {

    public static final String rssItem = "<item>\n" +
            "  <title>xxtitlexx</title>\n" +
            "  <link>xxlinkxx</link>\n" +
            "  <description>xxdescriptionxx</description>\n" +
            "</item>";

    public static  final String rssTemplate = "<?xml version=\"1.0\" ?>\n" +
            "<rss version=\"2.0\">\n" +
            "<channel>\n" +
            "</channel>\n" +
            "</rss>";

    private TOMLFile tomlFiLe;


    public FluxRss() {}

    public FluxRss(TOMLFile tomlFile) {
        this.tomlFiLe = tomlFile;
    }

    public void setTomlFiLe(TOMLFile t) {
        this.tomlFiLe = t;
    }

    public boolean trouveNewRss(){
        String toml = tomlFiLe.getContent();
        return toml.contains("newRss =");
    }

    public String rssChannel(String title,String link,String description){
        String s = "<channel>\n" +
                "    <title>"+title+"</title>\n" +
                "    <link>"+link+"</link>\n" +
                "    <description>"+description+"</description>\n" +
                "</channel>";
        return s;
    }

    public String itemRemplit(String item,String title,String link,String description){
        item = item.replace("xxtitlexx",title);
        item = item.replace("xxlinkxx",link);
        item = item.replace("xxdescriptionxx",description);
        return item;
    }

    public String addChannel(String rss,String Channel){
        rss = rss.replace( "<channel>\n" +
                "</channel>",Channel);
        return rss;
    }

    public String addFirstItem(String channel,String item){
        channel = channel.replace("</description>\n","</description>\n"+item+"\n");
        return channel;
    }

    public String addItem(String channel,String item){
        channel = channel.replace("</item>\n</channel>","</item>\n"+item+"\n</channel>");
        return channel;
    }

    public void rssGenerator(String path){
        if (!path.endsWith(".rss")){
            throw new IllegalArgumentException(path + " is not a rss file");
        }

        if(trouveNewRss()){
            TomlArray R = tomlGetArray("newRss");
            if(R.toList().size() < 3)
                System.out.println("informations manquantes");
            else {
                String rss = rssTemplate;
                String channel = rssChannel(R.toList().get(0).toString(), R.toList().get(1).toString(), R.toList().get(2).toString());
                String item = rssItem;
                int i = 1,j=1,z=1;
                for (Object x : R.toList()) {
                    if(z > 3) {
                        item = itemFiller(item,i,x);
                        if (i == 3) {
                            if (j == 1)
                                channel = addFirstItem(channel, item);
                            else
                                channel = addItem(channel, item);
                            i = 1;
                            j++;
                            item = rssItem;
                        } else {
                            i++;
                        }
                    }
                    z++;
                }
                rss = addChannel(rss, channel);
                fileWhrite(path,rss);
            }
        }else
            System.out.println("pas de beillets pour créer le flux rss ");
    }

    public void fileWhrite(String path, String s){
        try {
            File myObj = new File(path);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            throw new IllegalArgumentException("file creation failed ");
        }

        try {
            FileWriter myWriter = new FileWriter(path);
            myWriter.write(s);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            throw new IllegalArgumentException("file writing failed");
        }
    }

    public TomlArray tomlGetArray(String name){
        Path source = Paths.get(tomlFiLe.getPath().toString());
        TomlParseResult result = null;
        try {
            result = Toml.parse(source);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("failed to parse toml");
        }
        result.errors().forEach(error -> System.err.println(error.toString()));
        return result.getArray(name);
    }

    public String itemFiller(String item, int i , Object x){
        if (i == 1)
            item = itemRemplit(item, x.toString(), "xxlinkxx", "xxdescriptionxx");
        else if (i == 2)
            item = itemRemplit(item, "", x.toString(), "xxdescriptionxx");
        else if (i == 3)
            item = itemRemplit(item, "", "", x.toString());
        return item;
    }
}
