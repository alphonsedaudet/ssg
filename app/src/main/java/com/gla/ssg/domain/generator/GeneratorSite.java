package com.gla.ssg.domain.generator;

import com.gla.ssg.domain.metadata.MetaData;
import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.domain.template.Template;
import com.gla.ssg.port.statics.StaticHandler;
import com.gla.ssg.port.watchdog.WatchCallbackEvent;
import com.gla.ssg.util.Helpers;
import com.gla.ssg.util.IOService;
import org.tomlj.TomlParseResult;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class GeneratorSite extends Generator {

	private final String CONTENT = "content";
	private final String INDEX = "index.md";
	private final String STATIC = "static";
	private final String CONFIG = "site.toml";
	private final String TEMPLATE = "templates";

	private final Path outputDir;
	private final Path inputDir;
	private List<MarkdownFile> markdownFilesWrappers;

	public GeneratorSite(HTMLRenderer htmlRenderer, Path outputDir, Path inputDir, Boolean rebuildAll) {
		this.renderer = htmlRenderer;
		this.outputDir = outputDir;
		this.inputDir = inputDir;
		this.rebuildAll = rebuildAll;
	}

	@Override
	public Path getInputDir() {
		return this.inputDir;
	}

	@Override
	public void notifyChange(Path file, WatchCallbackEvent event) throws Exception {

		System.out.println("Event kind:" + event.toString() + ". File affected: " + file + ".");

		if (rebuildAll) {
			run();
			return;
		}

		if (event == WatchCallbackEvent.ENTRY_DELETE) {
			if (file.toString().endsWith(".md"))
				Helpers.changeFileExtension(Helpers.changeDirectory(file, outputDir), ".html").toFile().delete();
			else
				run();
		} else if ((event == WatchCallbackEvent.ENTRY_MODIFY) || (event == WatchCallbackEvent.ENTRY_CREATE)) {
			if (file.toString().endsWith(".toml") || file.endsWith(".html")) {
				run();
			} else if (file.toString().endsWith(".md")) {
				processMarkdownFiles(List.of(file));
			}
		}
	}

	private synchronized void processMarkdownFiles(List<Path> pahts) throws Exception {
		final List<File> directories = Arrays.asList(Objects.requireNonNull(inputDir.toFile().listFiles()));
		markdownFilesWrappers = readMarkdownFiles(pahts).stream().filter(this::isNotDraft).collect(Collectors.toList());
		setConfigData(markdownFilesWrappers);
		if (hasTemplateDirectory(directories))
			generateWithTemplate(directories);
		else
			generateWithoutTemplate(directories);
	}

	public void run() throws Exception {
		List<File> directories = Arrays.asList(Objects.requireNonNull(inputDir.toFile().listFiles()));
		File contentDirectory = getContentDirectory(directories);
		List<Path> markdownPaths = getMarkdownPaths(contentDirectory);
		hasIndexFile(markdownPaths);
		processMarkdownFiles(markdownPaths);
	}

	private boolean hasTemplateDirectory(List<File> directories) {
		final Optional<File> first = directories.stream().filter(file -> file.getName().equals(TEMPLATE)).findFirst();
		return first.isPresent();
	}

	private void generateWithoutTemplate(List<File> directories) throws Exception {
		final List<HTMLFile> writableHtml = generateHTMLFiles(markdownFilesWrappers).stream()
				.map(file -> FileWrapperFactory.newHTMLFile(file.getPath(), file.getContent()))
				.collect(Collectors.toList());
		processStaticFiles(directories, writableHtml);
		writeHTMLFiles(writableHtml, outputDir);
	}

	private void generateWithTemplate(List<File> directories) throws Exception {
		setMarkdownFileTemplate(markdownFilesWrappers);
		// apply the template an produce the result html file wrapper
		List<HTMLFile> writableHtml = applyTemplate(markdownFilesWrappers);
		processStaticFiles(directories, writableHtml);
		writableHtml.forEach(this::makeValid);
		writeHTMLFiles(writableHtml, outputDir);
	}

	/**
	 * Make the HTMLFile wrappers by using the template from the given MarkdownFile
	 * wrappers
	 */
	private List<HTMLFile> applyTemplate(List<MarkdownFile> markdownFilesWrappers) throws Exception {
		final Template templateGenerator = new Template();
		final HTMLFile defaultTemplateWrapper = getTemplateFileWrapper("default.html");
		List<HTMLFile> writableHtml = new ArrayList<>();
		for (MarkdownFile md : markdownFilesWrappers) {
			if (md.getTemplateFileWrapper().isPresent()) {
				templateGenerator.setTemplateFile(md.getTemplateFileWrapper().get());
			} else {
				if (defaultTemplateWrapper == null)
					throw new Exception("Not default template specified.");
				templateGenerator.setTemplateFile(defaultTemplateWrapper);
			}
			templateGenerator.setHTMLContent(getHTMLRenderer().render(md.getContent()));
			templateGenerator.setMarkdownFile(md);
			writableHtml.add(templateGenerator.build());
		}
		return writableHtml;
	}

	/**
	 * For each markdown file, set the template file if it has a specific template
	 * attribute.
	 */
	private void setMarkdownFileTemplate(List<MarkdownFile> markdownFiles) throws Exception {
		for (int i = 0; i < markdownFiles.size(); i++) {
			final MarkdownFile md = markdownFiles.get(i);
			if (md.getMetadataParseResult().isEmpty())
				continue;
			final String templateName = md.getMetadataParseResult().get().getString("template");
			if (templateName == null)
				continue;
			final HTMLFile templateFile = getTemplateFileWrapper(templateName);
			if (templateFile == null)
				md.setTemplateFileWrapper(Optional.empty());
			else
				md.setTemplateFileWrapper(Optional.of(templateFile));
			markdownFiles.set(i, md);
		}
	}

	private void makeValid(HTMLFile htmlFile) {
		htmlFile.setContent("<!DOCTYPE html>\n" + htmlFile.getContent());
	}

	private HTMLFile getTemplateFileWrapper(String fileName) throws Exception {
		Path path = Paths
				.get(inputDir.toFile().getAbsolutePath() + File.separator + TEMPLATE + File.separator + fileName);
		if (!Files.exists(path))
			return null;
		return FileWrapperFactory.newHTMLFile(path, IOService.readFromFile(path));
	}

	/**
	 * Handle the files that are not inside the contentDirectory but need to be part
	 * of the outputDir:
	 * <ul>
	 * <li>Files in the static folder
	 * <li>Links target when the target is not in the content directory.
	 * </ul>
	 */
	private void processStaticFiles(List<File> directories, List<HTMLFile> writableHtml) throws Exception {
		handleLinkTarget(writableHtml);
		final Optional<File> staticDirectory = getStaticDirectory(directories);
		if (staticDirectory.isPresent())
			StaticHandler.copyTree(staticDirectory.get().toPath().toString(), outputDir.toString());
	}

	private void handleLinkTarget(List<HTMLFile> writableHtml) throws Exception {
		for (HTMLFile file : writableHtml) {
			final List<String> hrefValues = MetaData.getHTMLAttributeValues("href", file);
			for (String url : hrefValues) {
				if (Helpers.isValidUrl(url))
					continue;
				final Optional<HTMLFile> first = writableHtml.stream()
						.filter(f -> f.getPath().toFile().getName().equals(url)).findFirst();
				if (first.isPresent())
					continue;
				final Optional<Path> targetPath = IOService.findFile(url, inputDir.toFile());
				if (targetPath.isEmpty())
					throw new Exception(url + " not found");
				// TODO check if the HTML is valid.
				StaticHandler.copierFichier(targetPath.get().toString(), outputDir.toString());
			}
		}
	}

	private Optional<File> getStaticDirectory(List<File> directories) {
		return directories.stream().filter(dir -> dir.getName().equals(STATIC)).findFirst();
	}

	private void hasIndexFile(List<Path> markdownPaths) throws Exception {
		final Optional<Path> first = markdownPaths.stream().filter(path -> path.getFileName().toString().equals(INDEX))
				.findFirst();
		if (first.isEmpty())
			throw new Exception("[content] directory must have index.md file");
	}

	private List<Path> getMarkdownPaths(File directory) {
		return Arrays.stream(Objects.requireNonNull(directory.listFiles()))
				.filter(file -> file.getName().endsWith(".md")).map(file -> Paths.get(file.getAbsolutePath()))
				.collect(Collectors.toList());
	}

	private File getContentDirectory(List<File> directories) throws Exception {
		final Optional<File> optionalContent = directories.stream().filter(dir -> dir.getName().equals(CONTENT))
				.findFirst();
		if (optionalContent.isEmpty())
			throw new Exception("No [content] directory found in " + inputDir.getFileName());
		return optionalContent.get();
	}

	private void setConfigData(List<MarkdownFile> mdFiles) throws Exception {
		final List<File> directories = Arrays.asList(Objects.requireNonNull(inputDir.toFile().listFiles()));
		final Optional<File> optionalConfig = directories.stream()
				.filter(dir -> dir.getName().equals(CONFIG) && dir.isFile()).findFirst();
		if (optionalConfig.isEmpty())
			return;
		final Path path = optionalConfig.get().toPath();
		final TomlParseResult TOMLParseResult = MetaData
				.parseTOMLFile(FileWrapperFactory.newTOMLFile(path, IOService.readFromFile(path)));
		mdFiles.forEach(mdWrapper -> mdWrapper.setConfigParseResult(Optional.of(TOMLParseResult)));
	}
}