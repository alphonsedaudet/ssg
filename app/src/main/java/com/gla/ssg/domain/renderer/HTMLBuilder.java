package com.gla.ssg.domain.renderer;

import java.util.AbstractMap;

public class HTMLBuilder {
    protected HTMLElement root = new HTMLElement();

    public HTMLBuilder withName(String name) {
        root.setName(name);
        return this;
    }

    public HTMLBuilder withText(String text) {
        root.setText(text);
        return this;
    }

    public HTMLBuilder withIndent(int size) {
        root.setIndentSize(size);
        return this;
    }

    public HTMLAttributeBuilder attributes() {
        return new HTMLAttributeBuilder(root);
    }

    public HTMLChildBuilder children() {
        return new HTMLChildBuilder(root);
    }

    public HTMLElement build() {
        return root;
    }

    public HTMLBuilder reset() {
        root = new HTMLElement();
        return this;
    }

}

class HTMLAttributeBuilder extends HTMLBuilder {
    public HTMLAttributeBuilder(HTMLElement root) {
        this.root = root;
    }

    public HTMLAttributeBuilder withAttribute(String name, String value) {
        root.getAttributes().add(new AbstractMap.SimpleEntry<>(name, value));
        return this;
    }
}

class HTMLChildBuilder extends HTMLBuilder {
    public HTMLChildBuilder(HTMLElement root) {
        this.root = root;
    }

    public HTMLChildBuilder addChild(HTMLElement element) {
        element.setIndentSize(root.getIndentSize());
        root.getChildren().add(element);
        return this;
    }

    public HTMLChildBuilder addChild(String childName, String childText) {
        HTMLElement e = new HTMLElement(childName, childText);
        return addChild(e);
    }
}

