package com.gla.ssg.domain.generator;

import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.port.watchdog.WatchCallbackEvent;
import com.gla.ssg.util.Helpers;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GeneratorFiles extends Generator {

	private final Path outputDir;
	private final List<Path> files;

	public GeneratorFiles(HTMLRenderer renderer, Path outputDir, List<Path> files, Boolean rebuildAll) {
		this.renderer = renderer;
		this.outputDir = outputDir;
		this.files = files;
		this.rebuildAll = rebuildAll;
	}

	@Override
	public Path getInputDir() {
		return Paths.get(System.getProperty("user.dir"));
	}

	public void run() throws Exception {
		validateFiles(files);
		List<Path> paths = files.stream().map(path -> Helpers.changeFileExtension(path, ".md"))
				.collect(Collectors.toList());

		for (Path path : paths) {
			try {
				writeFile(readMarkdownAndGenerateHtml(path), outputDir);
			} catch (Exception e) {
				throw new Exception(e.getMessage());
			}
		}
	}

	private void validateFiles(List<Path> files) throws IllegalArgumentException {
		Optional<Path> unexpected = files.stream().filter(path -> !path.getFileName().toString().endsWith(".html"))
				.findFirst();
		if (unexpected.isPresent())
			throw new IllegalArgumentException(
					unexpected.get().getFileName() + " should be a HTML file name with .html extension.");
	}

	@Override
	public void notifyChange(Path file, WatchCallbackEvent event) throws Exception {
		System.out.println("Event kind:" + event.toString() + ". File affected: " + file + ".");
		if (file.toString().endsWith(".md")) {
			if (event == WatchCallbackEvent.ENTRY_DELETE) {
				Helpers.changeFileExtension(Helpers.changeDirectory(file, outputDir), ".html").toFile().delete();
			} else {
				if (rebuildAll)
					run();
				else
					writeFile(readMarkdownAndGenerateHtml(file), outputDir);
			}
		}
	}

}
