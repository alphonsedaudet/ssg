package com.gla.ssg.domain.renderer;

import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.util.Helpers;
import org.commonmark.node.Node;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.tomlj.TomlParseResult;

import java.nio.file.Path;
import java.util.Optional;

public class HTMLRenderer {

    private String title = "";
    private String author = "";

    public HTMLFile markdownToHTML(MarkdownFile markdownFile) {
        Path name = Helpers.changeFileExtension(markdownFile.getPath(), ".html");
        String htmlContent = render(markdownFile.getContent());
        setConfig(markdownFile);
        setMetadata(markdownFile);
        return makeValidHTML(FileWrapperFactory.newHTMLFile(name, htmlContent));
    }

    /**
     * Compile content from Markdown (CommonMark) to HTML.
     */
    public String render(String content) {
        Parser parser = Parser.builder().build();
        Node document = parser.parse(content);
        HtmlRenderer renderer = HtmlRenderer.builder().build();
        return renderer.render(document);
    }

    private void setMetadata(MarkdownFile markdownFile) {
        final Optional<TomlParseResult> metadataParseResult = markdownFile.getMetadataParseResult();
        if (metadataParseResult.isPresent()) {
            final TomlParseResult TOMLParseResult = metadataParseResult.get();
            final String dataTitle = TOMLParseResult.getString("title");
            final String dataAuthor = TOMLParseResult.getString("author");
            title = dataTitle == null ? "No given title" : dataTitle;
            author = dataAuthor == null ? "" : dataAuthor;
        }
    }

    private void setConfig(MarkdownFile markdownFile) {
        final Optional<TomlParseResult> configParseResult = markdownFile.getConfigParseResult();
        if (configParseResult.isPresent()) {
            final TomlParseResult TOMLParseResult = configParseResult.get();
            final String dataTitle = TOMLParseResult.getString("title");
            final String dataAuthor = TOMLParseResult.getString("author");
            title = dataTitle == null ? title : dataTitle;
            author = dataAuthor == null ? author : dataAuthor;
        }
    }

    public HTMLFile makeValidHTML(HTMLFile file) {
        HTMLBuilder htmlBuilder = new HTMLBuilder();
        HTMLElement charsetElement = htmlBuilder.withName("meta").attributes()
                .withAttribute("charset", "UTF-8").build();
        HTMLElement viewportElement = htmlBuilder.reset().withName("meta").attributes()
                .withAttribute("name", "viewport")
                .withAttribute("content", "width=device-width, initial-scale=1.0").build();
        HTMLElement authorElement = htmlBuilder.reset().withName("meta").attributes()
                .withAttribute("name", "author")
                .withAttribute("content", author).build();
        HTMLElement titleElement = htmlBuilder.reset().withName("title").withText(title).build();
        HTMLElement head = htmlBuilder.reset().withName("head").children()
                .addChild(charsetElement).addChild(viewportElement).addChild(authorElement)
                .addChild(titleElement).build();
        HTMLElement body = htmlBuilder.reset().withName("body")
                .withText(String.format("%s%s", System.lineSeparator(), file.getContent()))
                .build();

        HTMLElement html = htmlBuilder.reset().withName("html")
                .attributes().withAttribute("lang", "fr")
                .children().addChild(head).addChild(body)
                .build();
        file.setContent(new HTMLDocument("<!DOCTYPE html>", html).toString());
        return file;
    }
}
