package com.gla.ssg.domain.template;

import com.gla.ssg.domain.generator.Generator;
import com.gla.ssg.domain.generator.GeneratorFiles;
import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.util.IOService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class TemplateTest {
    @Mock
    //GeneratorSite generator;

    Path sharedTempDir;

    Template underTest = new Template();

    @BeforeEach
    void setUp() {
        try {
            sharedTempDir = Files.createTempDirectory("dir");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Should return an empty map")
    void shouldReturnAnEmptyMap() {
        final MarkdownFile markdownFile = FileWrapperFactory.newMarkdownFile(sharedTempDir.resolve("file.md"), "");
        underTest.setMarkdownFile(markdownFile);
        assertTrue(underTest.getData().isEmpty());
    }

    @Test
    @DisplayName("Should throw an exception when the file to include does not exists")
    void shouldThrowAnExceptionWhenTheFileToIncludeDoesNotExists() {
        final HTMLFile template = FileWrapperFactory.newHTMLFile(sharedTempDir.resolve("file.html"),
                "{{ include \"test\" }}");
        underTest.setTemplateFile(template);
        try {
            assertThrows(Exception.class, () -> underTest.setInclude());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should return a map with the metadata key value paired")
    void shouldReturnAMapWithTheMetadataKeyValuePaired() {
        try {
            Path file = sharedTempDir.resolve("file.md");
            file = Files.write(file, Collections.singletonList("+++\n" + "title = \"my title\"\n"
                    + "date = 2021-01-13\n" + "categories = [{ name = \"Accueil\", url = \"index.html\"}]" + "+++\n"));
            final MarkdownFile mdFile = new GeneratorFiles(
                    new HTMLRenderer(),
                    sharedTempDir,
                    Collections.emptyList(),false).readMarkdownFile(file);
            underTest.setMarkdownFile(mdFile);

            final Map<String, Object> data = underTest.getData();

            assertAll(
                    () -> assertTrue(data.containsKey("title")), () -> assertTrue(data.containsKey("date")),
                    () -> assertTrue(data.containsKey("categories")));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should include the specified file")
    void shouldIncludeTheSpecifiedFile() {
        final HTMLFile template = FileWrapperFactory.newHTMLFile(sharedTempDir.resolve("file.html"),
                "{{ include \"test\" }}");
        final Path test = sharedTempDir.resolve("test");
        try {
            IOService.writeToFile(test, "Hello world");
            underTest.setTemplateFile(template);
            underTest.setInclude();
            assertEquals("Hello world\n", underTest.getTemplateContent());
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Disabled
    @Test
    @DisplayName("Should return the template with substituted metadata")
    void shouldReturnTheTemplateWithSubstitutedMetadata() {
        try {
            Path dir = Files.createTempDirectory("dir");
            Path inputDir = Files.createDirectory(dir.resolve("input"));
            Path content = Files.createDirectory(inputDir.resolve("content"));
            Path posts = Files.createDirectory(content.resolve("posts"));
            Files.createFile(posts.resolve("post1"));
            Files.createFile(posts.resolve("post2"));
            Path templates = Files.createDirectory(inputDir.resolve("templates"));
            Path index = content.resolve("index.md");
            Path template = templates.resolve("landing.html");

            IOService.writeToFile(index, "+++\n" +
                    "categories = [{ name = \"À propos\", url = \"about.html\" }]\n" +
                    "title = \"La Conquete du pain - Boulangerie\"\n+++" +
                    "# Home\nBienvenue sur le site de la boulangerie _La Conquête du pain_.");
            IOService.writeToFile(template, "<html>\n" +
                    "  <head>\n" +
                    "    <title>{{ metadata.title }}</title>\n" +
                    "  </head>\n" +
                    "  <body>\n" +
                    "    <h1>{{ metadata.title }}</h1>\n" +
                    "    <div id=\"article\">\n{{ content }}</div>\n" +
                    "    {% for category in metadata.categories %}" +
                    "      <li><a href=\"{{ category.url }}\">{{ category.name }}</a></li>\n" +
                    "    {% endfor %}" +
                    "    {% for post in list_files(\"posts\", true) %}" +
                    "      <li>{{ post }}</li>\n" +
                    "    {% endfor %}" +
                    "  </body>\n" +
                    "</html>\n");

            Generator generator = new GeneratorFiles(new HTMLRenderer(), sharedTempDir, Collections.emptyList(),false);
            final MarkdownFile mdFile = generator.readMarkdownFile(index);

            underTest.setMarkdownFile(mdFile);
            underTest.setTemplateFile(FileWrapperFactory.newHTMLFile(template, IOService.readFromFile(template)));
            underTest.setHTMLContent(generator.getHTMLRenderer().render(mdFile.getContent()));

            assertEquals("<html>\n" +
                    " <head>\n" +
                    " <title>La Conquete du pain - Boulangerie</title>\n" +
                    " </head>\n" +
                    " <body>\n" +
                    " <h1>La Conquete du pain - Boulangerie</h1>\n" +
                    " <div id=\"article\">\n" +
                    "<h1>Home</h1>\n" +
                    "<p>Bienvenue sur le site de la boulangerie <em>La Conquête du pain</em>.</p>\n" +
                    "</div>\n" +
                    "  <li><a href=\"about.html\">À propos</a></li>\n" +
                    "   <li>post1</li>\n" +
                    "  <li>post2</li>\n" +
                    "  </body>\n" +
                    "</html>", underTest.build().getContent());

        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should throw an exception if there is a syntax error")
    void shouldThrowAnExceptionIfThereIsASyntaxError() {
        try {
            Path dir = Files.createTempDirectory("dir");
            Path inputDir = Files.createDirectory(dir.resolve("input"));
            Path content = Files.createDirectory(inputDir.resolve("content"));
            Path templates = Files.createDirectory(inputDir.resolve("templates"));
            Path index = content.resolve("index.md");
            Path template = templates.resolve("landing.html");

            IOService.writeToFile(index, "+++\n" +
                    "title = \"La Conquete du pain - Boulangerie\"\n+++\n# Home\n");
            IOService.writeToFile(template, "<html>\n" +
                    "  <head>\n" +
                    "    <title>{{ metadata.title }</title>\n" +
                    "  </head>\n" +
                    "  <body>\n" +
                    "    <h1>{{ metadata.title }}</h1>\n" +
                    "  </body>\n" +
                    "</html>\n");

            Generator generator = new GeneratorFiles(new HTMLRenderer(), sharedTempDir, Collections.emptyList(),false);
            final MarkdownFile mdFile = generator.readMarkdownFile(index);

            underTest.setMarkdownFile(mdFile);
            underTest.setTemplateFile(FileWrapperFactory.newHTMLFile(template, IOService.readFromFile(template)));
            underTest.setHTMLContent(generator.getHTMLRenderer().render(mdFile.getContent()));

            assertThrows(Exception.class, () -> underTest.build());


        } catch (Exception e) {
            e.printStackTrace();
            fail(e.getMessage());
        }
    }
}
