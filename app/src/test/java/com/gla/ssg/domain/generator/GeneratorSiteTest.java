package com.gla.ssg.domain.generator;

import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.util.IOService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GeneratorSiteTest {

	@Mock
	HTMLRenderer renderer;

	@TempDir
	Path tempDir;

	Path inputDir;
	Path outputDir;

	GeneratorSite underTest;

	@BeforeEach
	void setUp() {
		try {
			inputDir = Files.createDirectory(tempDir.resolve("input"));
			outputDir = Files.createDirectory(tempDir.resolve("outpu"));
			underTest = new GeneratorSite(renderer, outputDir, inputDir, false);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("Should throw exception if no content directory")
	void shouldThrowExceptionIfNoContentDirectory() {
		underTest = new GeneratorSite(renderer, outputDir, inputDir, false);
		assertThrows(Exception.class, () -> underTest.run());
	}

	@Test
	@DisplayName("Should throw an exception when no index file in content directory")
	void shouldThrowAnExceptionWhenNoIndexFileInContentDirectory() {
		try {
			Files.createDirectory(Paths.get(inputDir + File.separator + "content"));
			underTest = new GeneratorSite(renderer, outputDir, inputDir, false);
			assertThrows(Exception.class, () -> underTest.run());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should build site")
	void shouldBuildSite() throws IOException {
		Path contentDir = Files.createDirectory(inputDir.resolve("content"));
		Path file = contentDir.resolve("index.md");
		IOService.writeToFile(file,
				"The paragraph starts here...\n" + "...and doesn't end until here.\n" + "# An H1 Header\n"
						+ "## An H2 Header\n" + "### An H3 Header\n" + "#### An H4 Header\n" + "- Item 1\n"
						+ "  - Sub-item 1\n" + "  - Sub-item 2\n" + "- Item 2\n" + "  - Sub-item 1");

		// TODO: check why the test fails when we use the mock?
		underTest = new GeneratorSite(new HTMLRenderer(), outputDir, inputDir, false);

		try {
			underTest.run();
			assertTrue(Files.exists(outputDir.resolve("index.html")));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}
}