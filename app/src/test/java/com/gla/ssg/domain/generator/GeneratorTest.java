package com.gla.ssg.domain.generator;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.HTMLFile;
import com.gla.ssg.domain.model.MarkdownFile;
import com.gla.ssg.domain.renderer.HTMLRenderer;

@ExtendWith(MockitoExtension.class)
public class GeneratorTest {

	@TempDir
	static Path sharedTempDir;

	@Mock
	HTMLRenderer renderer;

	Generator underTest;

	@BeforeEach
	void setUp() {
		underTest = new GeneratorFiles(renderer, sharedTempDir, List.of(sharedTempDir),false);
	}

	@Test	
	@DisplayName("Should return an empty list")
	void shouldReturnAnEmptyList() {
		final List<HTMLFile> result;
		try {
			result = underTest.generateHTMLFiles(Collections.emptyList());
			assertTrue(result.isEmpty());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should read files from disk and return FileWrapperList")
	void shouldReadFilesFromDiskAndReturnFileWrapperList() {
		Path file = sharedTempDir.resolve("file.md");
		try {
			Files.write(file, Collections.singletonList("1. one\n2. two\n3. three"));
			final List<MarkdownFile> mdFiles = underTest.readMarkdownFiles(Collections.singletonList(file));

			assertAll(
					() -> assertEquals(file, mdFiles.get(0).getPath()),
					() -> assertEquals("1. one\n2. two\n3. three\n", mdFiles.get(0).getContent()));

		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should write FileWrapper content on disk")
	void shouldWriteFileWrapperContentOnDisk() {
		Path file = sharedTempDir.resolve("file.html");
		String html = "<!DOCTYPE html><html lang='fe'><head></head><body><h1>TEST</h1></body></html>";
		try {
			underTest.writeHTMLFiles(Collections.singletonList(FileWrapperFactory.newHTMLFile(file, html)),
					sharedTempDir);
			assertEquals(html, Files.readString(file));
		} catch (IOException e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should throw IOException when file does not exists")
	void shouldThrowIOExceptionWhenFileDoesNotExists() {
		assertThrows(IOException.class,
				() -> underTest.readMarkdownFiles(Collections.singletonList(Paths.get("non-existing-file.md"))));
	}

	@Test
	@DisplayName("Should throw IllegalArgumentException when the given file is not a markdown file")
	void shouldThrowIllegalArgumentExceptionWhenTheGivenFileIsNotAMarkdownFile() {
		Path file = sharedTempDir.resolve("wrong-md-file-extension");
		assertThrows(IllegalArgumentException.class,
				() -> underTest.readMarkdownFiles(Collections.singletonList(file)));
	}

	@Test
	@DisplayName("Should use HTMLRender to generate HTMLFile")
	void shouldUseHtmlRenderToGenerateHtmlFile() {
		List<MarkdownFile> mdFiles = Collections
				.singletonList(FileWrapperFactory.newMarkdownFile(Paths.get("test.md"), "===\n"));
		List<HTMLFile> htmlFiles = Collections
				.singletonList(FileWrapperFactory.newHTMLFile(Paths.get("test.html"), "<p>===</p>\n"));

		when(renderer.markdownToHTML(mdFiles.get(0))).thenReturn(htmlFiles.get(0));

		final List<HTMLFile> result;
		try {
			result = underTest.generateHTMLFiles(mdFiles);
			verify(renderer).markdownToHTML(mdFiles.get(0));
			assertEquals(htmlFiles, result);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should return true if file has draft metadata set to false")
	void shouldReturnTrueIfFileHasDraftMetadataSetToFalse() {
		try {
			Path file = sharedTempDir.resolve("file.md");
			Files.write(file, Collections.singletonList("+++\ndraft = false\n+++\n1. one\n2. two\n3. three"));
			final List<MarkdownFile> mdFiles = underTest.readMarkdownFiles(Collections.singletonList(file));
			assertTrue(underTest.isNotDraft(mdFiles.get(0)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should return true if file has no draft metadata")
	void shouldReturnTrueIfFileHasNoDraftMetadata() {
		Path file1 = sharedTempDir.resolve("file1.md");
		Path file2 = sharedTempDir.resolve("file2.md");
		try {
			Files.write(file1, Collections.singletonList("1. one\n2. two\n3. three"));
			Files.write(file2, Collections.singletonList("+++\ntitle = \"title\"\n+++\n"));
			final List<MarkdownFile> mdFiles = underTest.readMarkdownFiles(Arrays.asList(file1, file2));
			assertAll(() -> assertTrue(underTest.isNotDraft(mdFiles.get(0))),
					() -> assertTrue(underTest.isNotDraft(mdFiles.get(1))));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should return false false if file has draft metadata set to true")
	void shouldReturnFalseFalseIfFileHasDraftMetadataSetToTrue() {
		try {
			Path file = sharedTempDir.resolve("file.md");
			Files.write(file, Collections.singletonList("+++\ndraft = true\n+++\n1. one\n2. two\n3. three"));
			final List<MarkdownFile> mdFiles = underTest.readMarkdownFiles(Collections.singletonList(file));
			assertFalse(underTest.isNotDraft(mdFiles.get(0)));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
