package com.gla.ssg.domain.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class FileWrapperFactoryTest {

    @Test
    @DisplayName("Should throw IllegalArgumentException")
    void shouldThrowIllegalArgumentException() {
        assertAll(
                () -> assertThrows(IllegalArgumentException.class, () -> FileWrapperFactory.newHTMLFile(Paths.get("test"))),
                () -> assertThrows(IllegalArgumentException.class, () -> FileWrapperFactory.newMarkdownFile(Paths.get("test"))),
                () -> assertThrows(IllegalArgumentException.class, () -> FileWrapperFactory.newTOMLFile(Paths.get("test")))
        );
    }

    @Test
    @DisplayName("Should create a FileWrapper with empty content")
    void shouldCreateAFileWrapperWithEmptyContent() {
        assertAll(
                () -> assertEquals("", FileWrapperFactory.newHTMLFile(Paths.get("test.html")).getContent()),
                () -> assertEquals("", FileWrapperFactory.newMarkdownFile(Paths.get("test.md")).getContent()),
                () -> assertEquals("", FileWrapperFactory.newTOMLFile(Paths.get("test.toml")).getContent())
        );
    }
}
