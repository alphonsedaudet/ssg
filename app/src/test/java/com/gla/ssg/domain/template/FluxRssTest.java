package com.gla.ssg.domain.template;

import com.gla.ssg.domain.model.TOMLFile;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.tomlj.TomlArray;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FluxRssTest {
    @TempDir
    static Path sharedTempDir;

    @Test
    void shouldReturnFileRssCreatedContent(){
        tomlFileCreate("test.toml","newRss =[\"test\",\"test\",\"test\"]");
        TOMLFile t = new TOMLFile(Path.of(sharedTempDir.toString() + "/test.toml"),"newRss =[\"test\",\"test\",\"test\"]");
        FluxRss f = new FluxRss(t);
        f.rssGenerator(sharedTempDir.toString()+"/test.rss");
        Path fileName = Path.of(sharedTempDir.toString()+"/test.rss");

        String actual = null;
        try {
            actual = Files.readString(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(actual, "<?xml version=\"1.0\" ?>\n" +
                "<rss version=\"2.0\">\n" +
                "<channel>\n" +
                "    <title>test</title>\n" +
                "    <link>test</link>\n" +
                "    <description>test</description>\n" +
                "</channel>\n" +
                "</rss>");
    }

    @Test
    void shouldReturnNullFileRssNotCreated (){
        tomlFileCreate("test.toml","");
        TOMLFile t = new TOMLFile(Path.of(sharedTempDir.toString() + "/test.toml"),"");
        FluxRss f = new FluxRss(t);
        f.rssGenerator(sharedTempDir.toString()+"/test.rss");
        Path fileName = Path.of(sharedTempDir.toString()+"/test.rss");

        String actual = null;
        try {
            actual = Files.readString(fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        assertEquals(actual, null);
    }

    @Test
    void shouldReturnArrayFromToml(){
        tomlFileCreate("test.toml","newRss =[\"test\",\"test\",\"test\"]");
        TOMLFile t = new TOMLFile(Path.of(sharedTempDir.toString() + "/test.toml"),"newRss =[\"test\",\"test\",\"test\"]");
        FluxRss f = new FluxRss(t);
        TomlArray A = f.tomlGetArray("newRss");
        String[] k = new String[3];
        k[0] = "test";
        k[1]= "test";
        k[2]="test";
        List<String> L = Arrays.asList(k);
        assertEquals(L,A.toList());

    }

    @Test
    void shouldReturnNUllArrayFromToml(){
        tomlFileCreate("test.toml","Title =\"test\"");
        TOMLFile t = new TOMLFile(Path.of(sharedTempDir.toString() + "/test.toml"),"newRss =[\"test\",\"test\",\"test\"]");
        FluxRss f = new FluxRss(t);
        TomlArray A = f.tomlGetArray("newRss");
        assertEquals(null,A);

    }

    @Test
    void shouldReturnTrueNewRssFound(){
        tomlFileCreate("test.toml","newRss =[\"test\",\"test\",\"test\"]");
        TOMLFile t = new TOMLFile(Path.of(sharedTempDir.toString() + "/test.toml"),"newRss =[\"test\",\"test\",\"test\"]");
        FluxRss f = new FluxRss(t);
        assertEquals(true,f.trouveNewRss());
    }

    @Test
    void shouldReturnFalseNewRssNotFound(){
        tomlFileCreate("test2.toml","title =\" test \" ");
        TOMLFile t = new TOMLFile(Path.of(sharedTempDir.toString() + "/test2.toml"),"title =\"test\" ");
        FluxRss f = new FluxRss(t);
        assertEquals(false,f.trouveNewRss());
    }

    void tomlFileCreate(String t,String s){
        try {
            File myObj = new File(sharedTempDir.toString()+"/"+t);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            throw new IllegalArgumentException("file creation failed ");
        }
        try {
            FileWriter myWriter = new FileWriter(sharedTempDir.toString()+"/"+t);
            myWriter.write(s);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
            throw new IllegalArgumentException("file writing failed");
        }
    }
}
