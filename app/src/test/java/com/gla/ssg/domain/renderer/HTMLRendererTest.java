package com.gla.ssg.domain.renderer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.nio.file.Paths;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import com.gla.ssg.domain.model.FileWrapper;
import com.gla.ssg.domain.model.MarkdownFile;


public class HTMLRendererTest {

    final HTMLRenderer underTest = new HTMLRenderer();

    @Test
    @DisplayName("Should render an ordered html list")
    void shouldRenderAnOrderedHtmlList() {
        final MarkdownFile mdFile = new MarkdownFile(Paths.get("file.md"), "1. foo\n" +
                "1. bar\n" +
                "1. baz\n");

        final FileWrapper htmlFile = underTest.markdownToHTML(mdFile);

        assertEquals("<!DOCTYPE html>\n" +
                        "<html lang=\"fr\">\n" +
                        "<head>\n" +
                        "<meta charset=\"UTF-8\">\n" +
                        "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                        "<meta name=\"author\" content=\"\">\n"+
                        "<title></title>\n" +
                        "</head>\n" +
                        "<body>\n" +
                        "<ol>\n" +
                        "<li>foo</li>\n" +
                        "<li>bar</li>\n" +
                        "<li>baz</li>\n" +
                        "</ol>\n" +
                        "</body>\n" +
                        "</html>\n",
                htmlFile.getContent());
    }


}
