package com.gla.ssg.domain.renderer;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class HTMLBuilderTest {

    @Test
    @DisplayName("Should return a well formed ul list")
    void shouldReturnAWellFormedUlList() {
        HTMLBuilder underTest = new HTMLBuilder();
        final HTMLElement actual = underTest
                .withName("ul").children()
                .addChild("li", "hello")
                .addChild("li", "Bonjour")
                .build();
        assertEquals("<ul>\n<li>hello</li>\n<li>Bonjour</li>\n</ul>\n", actual.toString());
    }

    @Test
    @DisplayName("Should return a well formed and indented ul list")
    void shouldReturnAWellFormedAndIndentedUlList() {
        HTMLBuilder underTest = new HTMLBuilder();
        final HTMLElement actual = underTest
                .withName("ul")
                .withIndent(2)
                .children()
                .addChild("li", "hello")
                .addChild("li", "Bonjour")
                .build();
        assertEquals("<ul>\n  <li>hello</li>\n  <li>Bonjour</li>\n</ul>\n", actual.toString());
    }

    @Test
    @DisplayName("Should return html element with attributes")
    void shouldReturnHtmlElementWithAttributes() {
        HTMLBuilder underTest = new HTMLBuilder();
        final HTMLElement actual = underTest
                .withName("a")
                .withText("foo")
                .attributes()
                .withAttribute("href", "www.foo.bar")
                .withAttribute("hidden", "true")
                .build();
        assertEquals("<a href=\"www.foo.bar\" hidden=\"true\">foo</a>\n", actual.toString());
    }

    @Test
    @DisplayName("Should return a well formed html using addChild with existing element")
    void shouldReturnAWellFormedHtmlUsingAddChildWithExistingElement() {
        HTMLElement span1 = new HTMLElement();
        span1.setName("span");
        span1.setText("span1");
        HTMLElement span2 = new HTMLElement("span");
        span2.setText("span2");
        HTMLElement span3 = new HTMLElement("span", "span3");
        HTMLBuilder underTest = new HTMLBuilder();
        final HTMLElement actual = underTest
                .withName("p")
                .withIndent(2)
                .children().addChild(span1).addChild(span2).addChild(span3).build();
        assertEquals("<p>\n  <span>span1</span>\n  <span>span2</span>\n  <span>span3</span>\n</p>\n",
                actual.toString());
    }
}
