package com.gla.ssg.domain.model;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class FileWrapperTest {
    @Test
    @DisplayName("Should throw IllegalArgumentException")
    void shouldThrowIllegalArgumentException() {
        assertThrows(IllegalArgumentException.class,
                () -> FileWrapperFactory.newTOMLFile(Paths.get("file.toml")).setPath(Paths.get("file")));
    }

    @Test
    @DisplayName("Should change the FileWrapper content")
    void shouldChangeTheFileWrapperContent() {
        FileWrapper htmlFile =  FileWrapperFactory.newHTMLFile(Paths.get("file.html"));
        htmlFile.setContent("new content");
        assertEquals("new content", htmlFile.getContent());
    }
}
