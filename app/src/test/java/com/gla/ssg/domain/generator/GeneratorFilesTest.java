package com.gla.ssg.domain.generator;

import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.util.IOService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class GeneratorFilesTest {

	@TempDir
	Path tempDir;
	Path outputDir;

	@Mock
	private HTMLRenderer renderer;

	@BeforeEach
	void setUp() {
		try {
			outputDir = Files.createTempDirectory(tempDir, "outputDir");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	@DisplayName("Should throw an exception when the given file is not a HTML file")
	void shouldThrowAnExceptionWhenTheGivenFileIsNotAHtmlFile() {
		try {
			Path file = tempDir.resolve("file");
			List<Path> files = Collections.singletonList(file);
			GeneratorFiles underTest = new GeneratorFiles(renderer, outputDir, files, false);
			assertThrows(Exception.class, () -> underTest.run());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should throw an exception when there is no corresponding md file for a given HTML file")
	void shouldThrowAnExceptionWhenThereIsNoCorrespondingMdFileForAGivenHtmlFile() {
		try {
			Path file = tempDir.resolve("file.html");
			List<Path> files = Collections.singletonList(file);

			GeneratorFiles underTest = new GeneratorFiles(renderer, outputDir, files, false);

			assertThrows(Exception.class, () -> underTest.run());
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

	@Test
	@DisplayName("Should generate the given html file in the specified output dir")
	void shouldGenerateTheGivenHtmlFileInTheSpecifiedOutputDir() {
		try {
			Path mdFile = tempDir.resolve("file.md");
			IOService.writeToFile(mdFile, "# Hello world");
			List<Path> files = Collections.singletonList(tempDir.resolve("file.html"));

			// new GeneratorFiles(renderer, outputDir, files).run(); test fail when we use
			// the mocked object
			new GeneratorFiles(new HTMLRenderer(), outputDir, files, false).run();

			assertTrue(Files.exists(outputDir.resolve("file.html")));
		} catch (Exception e) {
			fail(e.getMessage());
		}
	}

}
