package com.gla.ssg.util;

import com.gla.ssg.domain.model.TOMLFile;
import com.gla.ssg.domain.metadata.MetaData;
import com.gla.ssg.domain.model.FileWrapper;
import com.gla.ssg.domain.model.FileWrapperFactory;
import com.gla.ssg.domain.model.MarkdownFile;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.tomlj.TomlParseResult;
import picocli.CommandLine;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class HelpersTest {

    @TempDir
    static Path sharedTempDir;

    @ParameterizedTest
    @DisplayName("Should set the correct extension")
    @ValueSource(strings = {"file", "file.md", "file.html"})
    void shouldSetTheCorrectExtension(Path filename) {
        assertAll(
                () -> assertEquals(Paths.get("file.html"), Helpers.changeFileExtension(filename, ".html")),
                () -> assertEquals(Paths.get("file.md"), Helpers.changeFileExtension(filename, ".md")),
                () -> assertEquals(Paths.get("file.toml"), Helpers.changeFileExtension(filename, ".toml"))
        );
    }

    @Test
    @DisplayName("Should extract meta data from a markdown file")
    void shouldExtractMetaDataFromAMarkdownFile() {
        Path file = sharedTempDir.resolve("file.md");
        Path TOMLFile = sharedTempDir.resolve("file.toml");
        String metadata = "title = \"Souvenirs et aventures de ma vie\"\n" +
                "date = \"1903-02-13\"\n" +
                "author = \"Louise Michel\"\n";
        String content = "Lorem *ipsum* dolor sit amet.";
        final FileWrapper md = FileWrapperFactory.newMarkdownFile(file, "+++" + metadata + "+++" + content);

        try {
            MetaData.extractMetadata((MarkdownFile) md);
            assertAll(
                    () -> assertEquals(metadata, ((MarkdownFile) md).getMetadata().getContent()),
                    () -> assertEquals(TOMLFile, ((MarkdownFile) md).getMetadata().getPath()),
                    () -> assertEquals(content, md.getContent()));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should return a TomlResult from the parsing of the file content ")
    void shouldReturnATomlResultFromTheParsingOfTheFileContent() {
        Path file = sharedTempDir.resolve("file.toml");
        String metadata = "title = \"Souvenirs et aventures de ma vie\"\n" +
                "date = \"1903-02-13\"\n" +
                "author = \"Louise Michel\"\n";
        TOMLFile tomlFileWrapper = FileWrapperFactory.newTOMLFile(file, metadata);
        try {
            final TomlParseResult parseResult = MetaData.parseTOMLFile(tomlFileWrapper);
            assertAll(
                    () -> assertEquals("Souvenirs et aventures de ma vie", parseResult.getString("title")),
                    () -> assertEquals("1903-02-13", parseResult.getString("date")),
                    () -> assertEquals("Louise Michel", parseResult.getString("author")),
                    () -> assertNull(parseResult.getBoolean("draft"))
            );
        } catch (Exception e){
            fail(e.getMessage());
        }
    }


    @Test
    @DisplayName("Should return true when url is valid")
    void shouldReturnTrueWhenUrlIsValid() {
        assertTrue(Helpers.isValidUrl("https://gitlab.com/malek4/genie_logiciel_project"));
    }

    @Test
    @DisplayName("Should return false when url is not valid")
    void shouldReturnFalseWhenUrlIsNotValid() {
        assertFalse(Helpers.isValidUrl("unvalid-url.com"));
    }
}
