package com.gla.ssg.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IOServiceTest {

    @TempDir
    static Path sharedTempDir;

    @Test
    @DisplayName("Should throw FileNotFoundException")
    void shouldThrowFileNotFoundException() {
        assertThrows(FileNotFoundException.class, () -> IOService.readFromFile(Paths.get("unknown-file")));
    }

    @Test
    @DisplayName("Should write content to file")
    void shouldWriteContentToFile() {
        Path path = sharedTempDir.resolve("file.html");
        try {
            IOService.writeToFile(path, "<h1>hello</h1>");
            assertEquals("<h1>hello</h1>", Files.readString(path));
        } catch (IOException e) {
            fail("Should not throw an exception");
        }
    }

    @Test
    @DisplayName("Should return empty")
    void shouldReturnEmpty() {
        try {
            assertTrue(IOService.findFile("myFile", sharedTempDir.toFile()).isEmpty());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should return a non empty optional")
    void shouldReturnANonEmptyOptional() {
        Path file = sharedTempDir.resolve("file");
        try {
            Files.createFile(file);
            assertTrue(IOService.findFile("file", sharedTempDir.toFile()).isPresent());
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Should get all file names from the given directory and its sub-directories")
    void shouldGetAllFileNamesFromTheGivenDirectoryAndItsSubDirectories() {
        try {
            Path dir = Paths.get(sharedTempDir.toString() + File.separator + "dir");
            Path subDir = Paths.get(dir.toString() + File.separator + "subdir");
            Files.createDirectory(dir);
            Files.createDirectory(subDir);
            final Path f1 = sharedTempDir.resolve(dir.toString() + File.separator + "file1");
            Files.createFile(f1);
            final Path f2 = sharedTempDir.resolve(subDir.toString() + File.separator + "file2");
            Files.createFile(f2);
            final List<String> files = IOService.getFiles(dir.toFile(), true);
            files.forEach(System.out::println);
            assertAll(
                    () -> assertTrue(files.contains(f1.toString())),
                    () -> assertTrue(files.contains(f2.toString())),
                    () -> assertEquals(2, files.size()));
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }
}
