package com.gla.ssg.port.http;

import org.junit.jupiter.api.*;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.ServerSocket;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

class HttpServerTest {
    int port = 6000;
    ServerSocket serverConnect = new ServerSocket(port);

    HttpServerTest() throws IOException {
    }

    @BeforeEach
    void setUp() throws IOException {
        String nameFile = "index.html";

        Path dir1 = Files.createTempDirectory("dir1");
        File indexFile = new File(dir1+File.separator+nameFile);

        indexFile.createNewFile();


        Thread t = new Thread(){

            public void run() {
                //ServerSocket serverConnect = null;
                try {

                    HttpServer httpServer = new HttpServer(serverConnect.accept(), dir1.toAbsolutePath(), true);
                    new Thread(httpServer).start();
                    serverConnect.close();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        };
        t.start();
    }

    @AfterEach
    void tearDown() throws IOException {
        serverConnect.close();
    }

    @Test
    @DisplayName("Should run the server")
    void shouldRunServer() throws IOException {

        HttpURLConnection connection = (HttpURLConnection) new URL("http://localhost:"+port+"/").openConnection();
        assertEquals(connection.getResponseCode(), 200);
    }

    @Test
    @DisplayName("Should run the server")
    void shouldThrowFileNotFoundException() throws IOException {

        HttpURLConnection connection = (HttpURLConnection) new URL("http://localhost:"+port+"/any").openConnection();
        assertEquals(connection.getResponseCode(), 404);
    }
}