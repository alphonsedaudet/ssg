package com.gla.ssg.port.command.watchdog;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.io.FileWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import com.gla.ssg.domain.generator.Generator;
import com.gla.ssg.domain.generator.GeneratorSite;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.port.watchdog.Watchdog;

class WatchdogTest {

	GeneratorSite underTest;

	@AfterEach
	void tearDown() {
	}

	@Test
	void should() throws Exception {
		String nameFile = "index.md";

		Path dir1 = Files.createTempDirectory("dir1");
		Path dir2 = Files.createTempDirectory("dir2");
		Path pathContent = Paths.get(dir1.toAbsolutePath().toString() + "/content");
		Path contentDir1 = Files.createDirectories(pathContent);
		contentDir1.resolve("content");
		File toCopyFile = new File(pathContent.toAbsolutePath().toString() + File.separator + nameFile);
		FileWriter myWriter = new FileWriter(toCopyFile);
		myWriter.write("## An H2 Header\n");
		myWriter.close();
		underTest = new GeneratorSite(new HTMLRenderer(), dir2.toAbsolutePath(), dir1.toAbsolutePath(),false);
		underTest.run();

		Watchdog.subscribeWatcher(underTest);

		Thread.sleep(1000);

		FileWriter myWriter2 = new FileWriter(toCopyFile);
		myWriter2.write("## An H2 Header\n");
		myWriter2.close();

		String result = "<!DOCTYPE html>\n" + "<html lang=\"fr\">\n" + "<head>\n" + "<meta charset=\"UTF-8\">\n"
				+ "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
				+ "<meta name=\"author\" content=\"\">\n" + "<title></title>\n" + "</head>\n" + "<body>\n"
				+ "<h2>An H2 Header</h2>\n" + "</body>\n" + "</html>\n";
		assertEquals(result, Files.readString(Paths.get(dir2 + File.separator + "index.html")));
	}
}