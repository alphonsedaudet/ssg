package com.gla.ssg.port.statics;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

class StaticTest {

    @Test
    @DisplayName("Should copie the file from directory 1 to directory 2")
    void shouldCopieFile () throws IOException {
        String nameFile = "file1.txt";

        Path dir1 = Files.createTempDirectory("dir1");
        Path dir2 = Files.createTempDirectory("dir2");

        File toCopyFile = new File(dir1+File.separator+nameFile);
        toCopyFile.createNewFile();

        StaticHandler.copierFichier(toCopyFile.getPath(), dir2.toAbsolutePath().toString());

        assertTrue(Files.exists(Paths.get(dir2+File.separator+nameFile)));
    }

    @Test
    @DisplayName("Should copie the tree from directory 1 to directory 2")
    void shouldCopieTree () throws IOException {
        String nameFile = "file1.txt";

        Path dir1 = Files.createTempDirectory("dir1");
        Path dir2 = Files.createTempDirectory("dir2");
        Path pathContent = Paths.get(dir1.toAbsolutePath().toString()+"/contents");
        Path contentDir1 = Files.createDirectories(pathContent);

        File toCopyFile = new File(contentDir1+File.separator+nameFile);
        toCopyFile.createNewFile();

        StaticHandler.copyTree(dir1.toAbsolutePath().toString(), dir2.toAbsolutePath().toString());

        assertTrue(Files.exists(Paths.get(dir2.toAbsolutePath().toString()+File.separator+"contents"+File.separator+nameFile)));
    }


}