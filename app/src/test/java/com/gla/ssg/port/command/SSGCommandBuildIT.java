package com.gla.ssg.port.command;

import com.gla.ssg.domain.generator.Generator;
import com.gla.ssg.domain.generator.GeneratorSite;
import com.gla.ssg.domain.renderer.HTMLRenderer;
import com.gla.ssg.util.Helpers;
import com.gla.ssg.util.IOService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class SSGCommandBuildIT {

    Generator generator;
    SSGCommand underTest;
    Path inputDir;
    Path outputDir;

    @TempDir
    Path tempDir;

    @BeforeEach
    void setUp() {
        try {
            inputDir = Files.createTempDirectory(tempDir, "ssg_input");
            outputDir = Files.createTempDirectory(tempDir, "ssg_output");
            final HTMLRenderer htmlRenderer = new HTMLRenderer();
            generator = new GeneratorSite(htmlRenderer, outputDir, inputDir,false);
            underTest = new SSGCommand(htmlRenderer, new String[]{});
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Build command should create target files and return zero")
    void buildCommandShouldCreateTargetFilesAndReturnZero() {
        try {
            //GIVEN
            Path file1 = inputDir.resolve("file1.md");
            Path file2 = inputDir.resolve("file2.md");
            IOService.writeToFile(file1, "# File1");
            IOService.writeToFile(file2, "# File2");

            //WHEN
            underTest.setArgs(new String[]{
                    "build",
                    Helpers.changeFileExtension(file1, ".html").toString(),
                    Helpers.changeFileExtension(file2, ".html").toString(),
                    "--output-dir", outputDir.toString()});
            int code = underTest.start();

            //ASSERT
            assertAll(
                    () -> assertEquals(0, code),
                    () -> assertTrue(Files.exists(outputDir.resolve("file1.html"))),
                    () -> assertTrue(Files.exists(outputDir.resolve("file2.html"))));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    @DisplayName("Build command should generate the site and return zero")
    void buildCommandShouldGenerateTheSiteAndReturnZero() {
        try {
            Path content = inputDir.resolve("content");
            Files.createDirectory(content);
            Path index = content.resolve("index.md");
            Files.createFile(index);
            IOService.writeToFile(index, "# Index");

            underTest.setArgs(new String[]{"build",
                    "--input-dir", inputDir.toString(),
                    "--output-dir", outputDir.toString()});
            int code = underTest.start();

            assertAll(
                    () -> assertEquals(0, code),
                    () -> assertTrue(Files.exists(outputDir.resolve("index.html"))));
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    @Disabled
    @Test
    @DisplayName("Build command should generate the site with a config file")
    void buildCommandShouldGenerateTheSiteWithAConfigFile() {
        try {
            Path content = inputDir.resolve("content");
            Path siteConfig = inputDir.resolve("site.toml");
            Files.createDirectory(content);
            Path index = content.resolve("index.md");

            IOService.writeToFile(index, "# Index\n");
            IOService.writeToFile(siteConfig, "[general]\n" +
                    "title = \"A Minimal Site\"\n" +
                    "author = \"The SSG Team\"");

            underTest.setArgs(new String[]{"build",
                    "--input-dir", inputDir.toString(),
                    "--output-dir", outputDir.toString()});
            int code = underTest.start();
            String indexStr = IOService.readFromFile(outputDir.resolve("index.html"));
            System.out.println(indexStr);
            assertAll(
                    () -> assertEquals(0, code),
                    () -> assertTrue(indexStr.contains("<title>A Minimal Site</title>")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Generator site should generate valide Html file with template")
    void generatorSiteShouldGenerateValideHtmlFileWithTemplate() {
        try {
            Path content = inputDir.resolve("content");
            Path templates = inputDir.resolve("templates");
            Path siteConfig = inputDir.resolve("site.toml");
            Files.createDirectory(content);
            Files.createDirectory(templates);
            IOService.writeToFile(siteConfig, "title = \"A Minimal Site\"\n" +
                    "author = \"The SSG Team\"");
            //content dir
            Path index = Paths.get(content.toString() + File.separator + "index.md");
            IOService.writeToFile(index, "+++\n" +
                    "title = \"Welcome to Mini-Templates\"\n" +
                    "date = 2021-01-13\n+++\n\n# Index\n\n" +
                    "This is the home page of the Mini-Templates site.");
            //templates dir
            Path about = templates.resolve("about.html");
            Path defaultTemplate = templates.resolve("default.html");
            Path menu = Paths.get(templates.toString() + File.separator + "menu.html");
            IOService.writeToFile(about, "<!DOCTYPE html>\n<html>\n  <head>\n<title>About</title>\n</head>\n" +
                    "  <body>\n    This is my wonderful website. I hope you like it.\n </body>\n" +
                    "</html>");
            IOService.writeToFile(defaultTemplate, "<html>\n" +
                    "  <head>\n<title>{{ metadata.title }}</title>\n </head>\n" +
                    "  <body>\n" +
                    "    <div id=\"menu\">{{ include \"menu.html\" }}</div>\n" +
                    "    <h1>{{ metadata.title }}</h1>\n" +
                    "    <h2>{{ metadata.date }}</h2>\n" +
                    "    <div id=\"article\">{{ content }}</div>\n" +
                    "  </body>\n" +
                    "</html>\n");

            IOService.writeToFile(menu, "<a href=\"index.html\">Index</a>\n" +
                    "<a href='about.html'>About</a>");

            underTest.setArgs(new String[]{"build",
                    "--input-dir", inputDir.toString(),
                    "--output-dir", outputDir.toString()});
            int code = underTest.start();

            System.out.println(IOService.readFromFile(outputDir.resolve("index.html")));

            assertEquals(0, code);
        } catch (IOException e) {
            fail(e.getMessage());
        }
    }

    private boolean validateHtmlFilesInDir(Path dir) {
        //TODO
        return true;
    }
}


